import csv
import matplotlib.pyplot as plt

asean_countries = [
    "Brunei Darussalam",
    "Cambodia",
    "Indonesia",
    "Lao People's Democratic Republic",
    "Malaysia",
    "Myanmar",
    "Philippines",
    "Singapore",
    "Thailand",
    "Viet Nam",
]

aseanCountries_pop = {}


def calculate_population(country, year, population):
    if country in asean_countries and year == "2014":
        aseanCountries_pop[country] = float(population)


def file_reader(filename):
    with open(filename, "r") as k:
        population_data = csv.DictReader(k)
        for data in population_data:
            year = data["Year"]
            population = data["Population"]
            country = data["Region"]
            calculate_population(country, year, population)


def draw_plot(dictionary):
    plt.bar(list(dictionary.keys()), list(dictionary.values()))
    plt.xticks(rotation=30, fontsize=8)
    plt.show()


def main():
    file_reader(r"/home/ubuntu/Projects/project3/population-estimates_csv.csv")
    draw_plot(aseanCountries_pop)


main()
