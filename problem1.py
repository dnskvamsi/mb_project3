import csv
import matplotlib.pyplot as plt


population_per_year = {}


def calulate_populatin_per_year(country, year, population):
    if country == "India":
        if year in population_per_year:
            population_per_year[year] += float(population)
        else:
            population_per_year[year] = float(population)


def file_reader(filename):
    with open(r"/home/ubuntu/Projects/project3/population-estimates_csv.csv", "r") as k:
        population_data = csv.DictReader(k)
        for data in population_data:
            year = data["Year"]
            population = data["Population"]
            country = data["Region"]
            calulate_populatin_per_year(country, year, population)


def draw_plot(dictionary):
    plt.figure(figsize=(20, 10))
    plt.xticks(rotation=90)
    plt.bar(list(dictionary.keys()), list(dictionary.values()))
    plt.title("India_Population_Over_Years")
    plt.xlabel("Years")
    plt.ylabel("Population")
    plt.show()


def main():
    file_reader(r"/home/ubuntu/Projects/project3/population-estimates_csv.csv")
    draw_plot(population_per_year)


main()
