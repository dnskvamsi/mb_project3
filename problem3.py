import csv
import matplotlib.pyplot as plt

saarc_countries = [
    "Afghanistan",
    "Bangladesh",
    "Bhutan",
    "India",
    "Maldives",
    "Nepal",
    "Pakistan",
    "Sri Lanka",
]
saarc_countries_population_overyears = {}


def calculate_population(country, year, population):
    if country in saarc_countries:
        if country in saarc_countries_population_overyears:
            saarc_countries_population_overyears[year] += float(population)
        else:
            saarc_countries_population_overyears[year] = float(population)


def file_reader(filename):
    with open(filename, "r") as k:
        population_data = csv.DictReader(k)
        for data in population_data:
            year = data["Year"]
            population = data["Population"]
            country = data["Region"]
            calculate_population(country, year, population)


def draw_plot(dictonary):
    plt.figure(figsize=(20, 10))
    plt.bar(list(dictonary.keys()), list(dictonary.values()))
    plt.xticks(rotation=90)
    plt.ylabel("population")
    plt.xlabel("years")
    plt.title("Total_Saarc_countries_population vs Years")
    plt.show()


def main():
    file_reader(r"/home/ubuntu/Projects/project3/population-estimates_csv.csv")
    draw_plot(saarc_countries_population_overyears)


main()
